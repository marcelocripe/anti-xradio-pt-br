# antiXradio
# Copyright (C) 2023 antiX Community
# This file is distributed under the same license as the antixradio package (GPL v.3)
# Wallon, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version:version 0.44\n"
"Report-Msgid-Bugs-To: https://www.antixforum.com\n"
"POT-Creation-Date: 2023-12-24 07:41+0100\n"
"PO-Revision-Date: 2023-12-24 11:24+0100\n"
"Last-Translator: Wallon\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2.2\n"

#. Text displayed in a button.
#: antiXradio:53
msgid "Exit"
msgstr ""

#. Text displayed in a button.
#: antiXradio:54
msgid "Play"
msgstr ""

#. Text displayed in a button.
#: antiXradio:55
msgid "Stop"
msgstr ""

#. Text displayed in a button.
#: antiXradio:56
msgid "Load another list"
msgstr ""

#. Text displayed in a button.
#: antiXradio:57
msgid "Turn off the radio"
msgstr ""

#. Text displayed in a button.
#: antiXradio:58
msgid "Return"
msgstr ""

#: antiXradio:59
msgid "Radio stations:"
msgstr ""

#: antiXradio:60
msgid "(Standby)"
msgstr ""

#. Text displayed in a button for all files with the ".txt" extension.
#: antiXradio:61
msgid "Text files"
msgstr ""

#: antiXradio:62
msgid "Select the radio list you want to load:"
msgstr ""

#. Text displayed in a button.
#: antiXradio:63
msgid "Close window"
msgstr ""

#: antiXradio:64
msgid "stopped"
msgstr ""

#. Text displayed in a button.
#: antiXradio:65
msgid "Record"
msgstr ""

#: antiXradio:66
msgid ""
"This will power off the radio and stop any running station or record. If you "
"want just to close this window instead, keeping the radio playing, use the X "
"in upper window border or the ESC key on your keyboard instead. To stop "
"reception later call antiXradio again, and press this button then."
msgstr ""

#: antiXradio:69
msgid ""
"This will allow you to select another stations list from a folder on your "
"file system. You may edit the stations lists within a text editor, just make "
"sure to keep the format you find in it."
msgstr ""

#: antiXradio:71
msgid ""
"Stop current radio reception or recording. Starting another station or "
"another recording will also stop the current reception."
msgstr ""

#: antiXradio:73
msgid ""
"Record what the selected station plays to a file in your home folder's "
"“Radio-Recordings” directory. The file will have .ts extension and can be "
"played with MPV media player. You may convert it after recording was "
"completed to whatever format (mp3, mka, ogg m4a) by means of a audio "
"conversion tool (e.g. ffmpeg or audacity)"
msgstr ""

#: antiXradio:77
msgid ""
"This will start reception of selected radio station. You need to be "
"connected to the internet to receive the radio streams via network."
msgstr ""

#: antiXradio:79
msgid "Stop radio broadcasts and recordings if any, exit antiXradio"
msgstr ""

#: antiXradio:80
msgid "Cancel action and return to the main window"
msgstr ""

#: antiXradio:81
msgid ""
"Listen to the radio station without distracting window. Launch the radio "
"later again from antiX main menu to change radio channel, stop radio "
"reception or recording, or to exit antiXradio."
msgstr ""

#. Base folder name for storing recordings (sub-folder of user's home folder)
#: antiXradio:94
msgid "Radio-Recordings"
msgstr ""

#. One or more software packages were not found
#: antiXradio:115
msgid "not found."
msgstr ""

#. Do not translate the "\n" code. These are line breaks. If your text is too long, you can add "\n" codes.
#: antiXradio:117
msgid ""
"The installation of this script has failed. Make sure the above "
"command(s)\\nis (are) available on your system and please contact package "
"maintainer.\\nantiXradio Leaving."
msgstr ""

#. translatable window header, used in main and info window both. I gues only the part "Radio" should get translated, since antiX is a name.
#: antiXradio:181 antiXradio:198 antiXradio:206 antiXradio:279 antiXradio:350
#: antiXradio:358 antiXradio:448 antiXradio:449 antiXradio:450 antiXradio:451
#: antiXradio:532 antiXradio:550
msgid "antiXradio"
msgstr ""

#. Do not translate the codes "\n\t" or "\n". These are line breaks. If your text is too long, you can add codes "\n\t" or "\n".
#. Never translate "$exit_button_text". Also <b> and </b> is not translatable, these are tags for bold text in between. The order
# of opening <b> and closing tags </b> MUST be observed properly.
#: antiXradio:182
#, sh-format
msgid ""
"\\n\\t<b> Please note! </b>\\n\\n\\t When using the ESC key or the X from "
"upper window\\n\\t borders, only the window will be closed while the\\n\\t "
"radio keeps playing (and/or recording).\\n\\t To power off the radio later, "
"please start antiX Radio \\n\\t and click the '<b>$exit_button_text</b>' "
"button on the lower \\n\\t left section of it's window. \\n\\n"
msgstr ""

#: antiXradio:183
msgid "Got it. Don’t show me again."
msgstr ""

#: antiXradio:224
msgid "Internet connection error."
msgstr ""

#: antiXradio:225
msgid "No Internet connection found."
msgstr ""

#. Do not translate the "\n" code. These are line breaks. If your text is
#. too long, you can add "\n" codes.
#: antiXradio:225
msgid ""
"Please connect to the Internet before\\ntrying again or you may exit "
"antiXradio."
msgstr ""

#. Text displayed in a button.
#: antiXradio:226
msgid "Exit antiXradio"
msgstr ""

#. Text displayed in a button.
#: antiXradio:226
msgid "Try again"
msgstr ""

#. Base Filename for recording, followed by a date
#: antiXradio:315
msgid "radio-recording"
msgstr ""

#. Title of yad notifiction in system's status bar.
#: antiXradio:332
msgid "antiXradio recording"
msgstr ""

#. Tooltip of notification icon in system's status bar, to be translated to the meaning "There is a recording running"
#: antiXradio:333 antiXradio:338
msgid "Running antiXradio recording"
msgstr ""

#. Checkbox label in main window
#: antiXradio:547
msgid ""
"Displays a small window with information on the current radio programme and "
"the station."
msgstr ""

#. Window title supplement in main window's upper border, present if a recording is running. Please keep the double exclamation mark for better perceptibility
#: antiXradio:583
msgid "!! Recording !!"
msgstr ""
